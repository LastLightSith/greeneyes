Noice and Kool Green Theme

## How to install

1. Install vsce
```bash
npm install -g vsce
```

1. Build the extention
```
vsce package
```

1. Install the extention

```
code --install-extension greeneyes-0.0.1.vsix
```
